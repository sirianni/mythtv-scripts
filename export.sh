#!/bin/bash

set -x

# See http://tech.surveypoint.com/posts/mythtv-transcoding-with-handbrake/
LOG=/var/log/mythtv/export.log

exec > $LOG 2>&1

DBUSER=mythtv
DBPASSWD=mythtv
OUTDIR=/mnt/video/tv-shows
TMPDIR=$(mktemp -d)
trap 'rm -rf $TMPDIR' EXIT

echo "Temporary directory: $TMPDIR"

CHANID=$1
STARTTIME=$2
FILE=$3
TITLE=$4
SUBTITLE=$5

echo "CHANID=$CHANID"
echo "STARTTIME=$STARTTIME"
echo "FILE=$FILE"
echo "TITLE=$TITLE"
echo "SUBTITLE=$SUBTITLE"


EPISODE=$(mysql --skip-column-names -u $DBUSER -p$DBPASSWD mythconverg <<EOF
    select concat('s',lpad(season,2,'0'),'e',lpad(episode,2,'0'))
    from recorded 
    where chanid=$CHANID and starttime=$STARTTIME
EOF
)

OUTFILE="$OUTDIR/$TITLE"
mkdir -m 777 -p "$OUTFILE"

if [[ $EPISODE != "s00e00" && "$SUBTITLE" != "" ]] ; then
    OUTFILE="$OUTFILE/${EPISODE} - $SUBTITLE"
elif [[ $EPISODE != "s00e00" ]] ; then
    OUTFILE="$OUTFILE/$EPISODE"
elif [[ "$SUBTITLE" != "" ]] ; then
    OUTFILE="$OUTFILE/$SUBTITLE"
else
    OUTFILE="$OUTFILE/$STARTTIME"
fi

OUTFILE="$OUTFILE.m4v"

echo "Generating cutlist..."
mythutil --chanid $CHANID --starttime $STARTTIME --gencutlist

echo "Removing commericals via lossless transcode..."
mythtranscode --chanid $CHANID --starttime $STARTTIME --mpeg2 --honorcutlist --outfile $TMPDIR/video.mpg

echo "Desination file: $OUTFILE"
echo "Transcoding to H.264 via Handbrake..."
HandBrakeCLI \
    --input $TMPDIR/video.mpg \
    --output "$OUTFILE" \
    --preset Normal \
    --quality 23 \
    --aencoder faac,copy \
    --decomb \
    --subtitle scan --subtitle-forced --subtitle-burned

